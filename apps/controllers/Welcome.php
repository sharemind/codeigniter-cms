<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index()
    {
        $data = array(
            'site_title'    => $this->config->item('site_name'),
            'app_path'      => (__FILE__),
        );
        $this->load->view('welcome_message',$data);
    }

}
